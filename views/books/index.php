<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success btn-sm pull-right']) ?>
    </h1>

    <hr>

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => function ($row) {
                    return "{$row->author->firstname} {$row->author->lastname}";
                },
            ],
            'date',
            [
                'attribute' => 'cover',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::getAlias('@web').'/uploads/'. $data['cover'], ['width' => '200px']);
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
