<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->errorSummary($model)?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author_id')
        ->dropDownList(
            ArrayHelper::map(\app\models\Authors::find()->all(), 'id', 'firstname')
        )
    ?>
    <?= $form->field($model, 'cover')->fileInput() ?>
    <?= $form->field($model, 'date')->widget(
        yii\jui\DatePicker::className(), ['dateFormat' => 'yyyy-M-d', 'options' => ['class' => 'form-control']]
    ) ?>

    <hr>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
