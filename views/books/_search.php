<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BooksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'class' => 'form-inline'
        ]
    ]); ?>

    <?= $form->field($model, 'author_id')
        ->dropDownList(
            ArrayHelper::map(\app\models\Authors::find()->all(), 'id', 'firstname')
        )
    ?>
    &nbsp;
    <?= $form->field($model, 'name') ?>
    <hr>
    <p>
    <?= $form->field($model, 'from')->widget(
        yii\jui\DatePicker::className(), ['dateFormat' => 'yyyy-M-d', 'options' => ['class' => 'form-control']]
    ) ?>

    &nbsp;

    <?= $form->field($model, 'to')->widget(
        yii\jui\DatePicker::className(), ['dateFormat' => 'yyyy-M-d', 'options' => ['class' => 'form-control']]
    ) ?>
    </p>

    <p><?= Html::submitButton('Найти', ['class' => 'btn btn-default btn-sm']) ?></p>

    <?php ActiveForm::end(); ?>

</div>
<hr>